
The data represents a growth of an (invasive) plant in a grid that represents the environment. The plant is glossy buckthorn, an invasive shrub in New England. It takes about 3-5 years for the shrub to mature and produce seeds. The information provided for each cell includes the number of plants and the number of seeds growing in the cell

Each row corresponds to a single year of data. The dataset includes results from multiple simulations with the same landscape.

Note that each cell in the landscape has different composition of landcovers which leads to different behaviors in terms of the growth and control of the plain.

The sequence of steps within a single year is:

1. The plants and seeds are counted
2. The control action is applied
3. Reward is computed
4. New seedlings sprout

To keep things simple, there are only 2 actions in the data right now:

- `0.`  No control action is applied
- `10.` Cutting and spraying is applied to all cells

Note that young seedlings have a high mortality rate, and thus it is possible for the population to decrease (when most plants are young) even without an action taken.

Columns
-------

- `V1`: Year of the current simulation. The steps from a single simulation are saved consequetively. If the V1 value is lower than the row above, that means that these values come from a new simulation.
- `pop_i`: The number of live and growing plans in cell i in the beginning of the year
- `sbank_i`: The number of seeds (in the ground) in cell i in the beginning of the year

Output
------

For me to evaluate your policy in the simulator, please supply one of the input files with an additional column `policy` that lists the action recommended by your policy.

